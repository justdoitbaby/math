#coding=utf-8
from django.db import models

# Create your models here.
class Director(models.Model):
    name = models.CharField(max_length = 20)
    img = models.ImageField(upload_to = 'images')
    job_title = models.CharField(max_length = 50)
    position = models.CharField(max_length = 50)
    dpartment = models.CharField(max_length = 50)
    phonenbr = models.CharField(max_length = 20)
    office_adress = models.CharField(max_length = 50)
    email = models.CharField(max_length = 50)
    personal_website = models.CharField(max_length =50)
    is_active = models.BooleanField(default = True)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']
        verbose_name = '主任'

    def __unicode__(self):
        return self.name

class Prefessor(models.Model):
    name = models.CharField(max_length = 20)
    img = models.ImageField(upload_to = 'images')
    job_title = models.CharField(max_length = 50)
    position = models.CharField(max_length = 50)
    dpartment = models.CharField(max_length = 50)
    phonenbr = models.CharField(max_length = 20)
    office_adress = models.CharField(max_length = 50)
    email = models.CharField(max_length = 50)
    personal_website = models.CharField(max_length =50)
    is_active = models.BooleanField(default = True)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']
        verbose_name = '教授'    
    def __unicode__(self):
        return self.name


class Teach_staff(models.Model):
    name = models.CharField(max_length = 20)
    img = models.ImageField(upload_to = 'images')
    job_title = models.CharField(max_length = 50)
    position = models.CharField(max_length = 50)
    dpartment = models.CharField(max_length = 50)
    phonenbr = models.CharField(max_length = 20)
    office_adress = models.CharField(max_length = 50)
    email = models.CharField(max_length = 50)
    personal_website = models.CharField(max_length =50)
    is_active = models.BooleanField(default = True)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']
        verbose_name = '教学人员'

    def __unicode__(self):
        return self.name

    
    
    
