# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('personnel', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='personnel',
            name='position',
            field=models.CharField(default=datetime.datetime(2016, 3, 10, 2, 38, 37, 996575, tzinfo=utc), max_length=50),
            preserve_default=False,
        ),
    ]
