# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personnel', '0004_auto_20160314_0218'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='director',
            options={'ordering': ['order'], 'verbose_name': '\u4e3b\u4efb'},
        ),
        migrations.AlterModelOptions(
            name='prefessor',
            options={'ordering': ['order'], 'verbose_name': '\u6559\u6388'},
        ),
        migrations.AlterModelOptions(
            name='teach_staff',
            options={'ordering': ['order'], 'verbose_name': '\u6559\u5b66\u4eba\u5458'},
        ),
    ]
