# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personnel', '0003_auto_20160311_0234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='director',
            name='img',
            field=models.ImageField(upload_to=b'images'),
        ),
        migrations.AlterField(
            model_name='prefessor',
            name='img',
            field=models.ImageField(upload_to=b'images'),
        ),
        migrations.AlterField(
            model_name='teach_staff',
            name='img',
            field=models.ImageField(upload_to=b'images'),
        ),
    ]
