# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Personnel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('img', models.ImageField(upload_to=b'')),
                ('job_title', models.CharField(max_length=50)),
                ('dpartment', models.CharField(max_length=50)),
                ('phonenbr', models.CharField(max_length=20)),
                ('office_adress', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('personal_website', models.CharField(max_length=50)),
                ('is_active', models.BooleanField(default=True)),
                ('order', models.IntegerField()),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
