#coding=utf-8
from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from Math.personnel.models import Director, Prefessor, Teach_staff 
# Create your views here.

def personnel(request):
    director_list = Director.objects.all()
    prefessor_list = Prefessor.objects.all()
    teach_staff_list = Teach_staff.objects.all()
    return render_to_response("personnel.html",{"director_list":director_list,"prefessor_list":prefessor_list,"teach_staff_list":teach_staff_list},RequestContext(request))

