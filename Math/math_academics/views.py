#coding:utf-8
from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from Math.math_academics.models import Basic_math, Apply_math

def math_academics(request):
    basic_math_list = Basic_math.objects.all()
    apply_math_list = Apply_math.objects.all()
    return render_to_response("math_academics.html",{"basic_math_list":basic_math_list,"apply_math_list":apply_math_list},RequestContext(request))

