from django.contrib import admin
from Math.math_academics.models import Basic_math, Apply_math


admin.site.register(Basic_math)
admin.site.register(Apply_math)
