#coding:utf-8
from django.db import models

class Basic_math(models.Model):
    name = models.CharField((u"name"),max_length = 20)
    sex = models.CharField(max_length = 10)
    job_title = models.CharField(max_length = 20)
    degree = models.CharField(max_length = 50)
    research_direction = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)

    class Meta:
        ordering = ['order']
        verbose_name = '基础数学'
        verbose_name_plural = '基础数学'

    def __unicode__(self):
        return self.name


class Apply_math(models.Model):
    name = models.CharField(max_length = 20)
    sex = models.CharField(max_length = 10)
    job_title = models.CharField(max_length = 20)
    degree = models.CharField(max_length = 50)
    research_direction = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)
    
    class Meta:
        ordering = ['order']
        verbose_name = '应用数学'
        verbose_name_plural =  '应用数学'

    def __unicode__(self):
        return self.name

class Probability_and_mathematical_statistic(models.Model):
    name = models.CharField((u"name"),max_length = 20)
    sex = models.CharField(max_length = 10)
    job_title = models.CharField(max_length = 20)
    degree = models.CharField(max_length = 50)
    research_direction = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)

    class Meta:
        ordering = ['order']
        verbose_name = '概率论与数理统计'
        verbose_name_plural = '概率论与数理统计'
    
    def __unicode__(self):
        return self.name

class PDE_math(models.Model):
    name = models.CharField((u"name"),max_length = 20)
    sex = models.CharField(max_length = 10)
    job_title = models.CharField(max_length = 20)
    degree = models.CharField(max_length = 50)
    research_direction = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)

    class Meta:
        ordering = ['order']
        verbose_name = '偏微分方程'
        verbose_name_plural = '偏微分方程'
    
    def __unicode__(self):
        return self.name

class Finacial_math(models.Model):
    name = models.CharField((u"name"),max_length = 20)
    sex = models.CharField(max_length = 10)
    job_title = models.CharField(max_length = 20)
    degree = models.CharField(max_length = 50)
    research_direction = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)

    class Meta:
        ordering = ['order']
        verbose_name = '金融数学'
        verbose_name_plural = '金融数学'

    def __unicode__(self):
        return self.name

