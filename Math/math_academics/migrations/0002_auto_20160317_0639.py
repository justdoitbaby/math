# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('math_academics', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Apply_math',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('sex', models.CharField(max_length=10)),
                ('job_title', models.CharField(max_length=20)),
                ('degree', models.CharField(max_length=50)),
                ('research_direction', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Basic_math',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('sex', models.CharField(max_length=10)),
                ('job_title', models.CharField(max_length=20)),
                ('degree', models.CharField(max_length=50)),
                ('research_direction', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='information',
            name='category',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
        migrations.DeleteModel(
            name='Information',
        ),
    ]
