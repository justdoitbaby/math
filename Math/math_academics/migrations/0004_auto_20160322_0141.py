# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('math_academics', '0003_auto_20160317_0936'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='apply_math',
            options={'ordering': ['order'], 'verbose_name': '\u5e94\u7528\u6570\u5b66'},
        ),
        migrations.AlterModelOptions(
            name='basic_math',
            options={'ordering': ['order'], 'verbose_name': '\u57fa\u7840\u6570\u5b66'},
        ),
    ]
