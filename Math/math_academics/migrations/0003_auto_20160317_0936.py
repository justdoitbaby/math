# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('math_academics', '0002_auto_20160317_0639'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basic_math',
            name='name',
            field=models.CharField(max_length=20, verbose_name='name'),
        ),
    ]
