"""Math URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
	Examples:
	Function views
	    1. Add an import:  from my_app import views
		    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
			Class-based views
			    1. Add an import:  from other_app.views import Home
				    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
					Including another URLconf
					    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
						"""


from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings

#from Math.index.views import index
#from Math.aboutus.views import aboutus
from Math.index.views import index

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$',index),
    url(r'^index/',include('Math.index.urls')),
    url(r'^aboutus/',include('Math.aboutus.urls')),
    url(r'^news/',include('Math.news.urls')),
    url(r'^academic/',include('Math.academic.urls')),
    url(r'^math_master/',include('Math.math_master.urls')),
    url(r'^math_bachelor/',include('Math.math_bachelor.urls')),
    url(r'^contact/',include('Math.contact.urls')),
    url(r'^employment/',include('Math.employment.urls')),
    url(r'^personnel/',include('Math.personnel.urls')),
    url(r'^math_academics/',include('Math.math_academics.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


#if settings.DEBUG:
 #   # static files (images, css, javascript, etc.)
  #  urlpatterns += patterns('',
   # (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
   # 'document_root': settings.MEDIA_ROOT}))

       
