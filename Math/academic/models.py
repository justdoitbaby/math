#coding:utf-8
from django.db import models

class Lecture(models.Model):
    date = models.DateTimeField()
    adress = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    lecturer = models.CharField(max_length = 20) 
    is_active = models.BooleanField(default =True)

    class Meta:
        ordering = ['-date']
        verbose_name = '学术讲座'

    def __unicode__(self):
        return self.title


