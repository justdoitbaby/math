#coding=utf-8
from django.conf.urls import include, url
from django.contrib import admin

from  Math.math_master import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$',views.math_master),
]

