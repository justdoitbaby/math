#coding=utf-8
from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from Math.math_master.models import Math_master

def math_master(request):
    math_master_list = Math_master.objects.all()
    return render_to_response("math_master.html",{"math_master_list":math_master_list},RequestContext(request))
