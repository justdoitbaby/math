# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('math_master', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='math_master',
            options={'verbose_name': '\u7814\u7a76\u751f\u57f9\u517b'},
        ),
    ]
