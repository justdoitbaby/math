#coding:utf-8
from django.db import models

class Column(models.Model):
    id = models.CharField(primary_key = True,max_length = 10)
    name = models.CharField(max_length = 50)
    url = models.CharField(null = True,max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)

    def __unicode__(self):
        return self.name

	#class Meta:
	#	ordering = ['order']


class Dpartments(models.Model):
    name = models.CharField(max_length = 50)
    url = models.CharField(null = True,max_length =50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)
    column = models.ForeignKey(Column)

    def __unicode__(self):
        return self.name


	#class Meta:
	#	ordering = ['order']


class Personal(models.Model):
    name = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)
    column = models.ForeignKey(Column)

    def __unicode__(self):
        return self.name


	#class Meta:
	#	ordering = ['order']

class Undergraduate(models.Model):
    name = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)
    column = models.ForeignKey(Column)

    def __unicode__(self):
        return self.name


	#class Meta
	#	ordering = ['order']


class Postgraduate(models.Model):
    name = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)
    column = models.ForeignKey(Column)

    def __unicode__(self):
        return self.name


	#class Meta:
	#	ordering = ['order']

class Academic(models.Model):
    name = models.CharField(max_length = 50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)
    column = models.ForeignKey(Column)

    def __unicode__(self):
        return self.name


	#class Meta:
	#	ordering = ['order']

class Activity(models.Model):
    name = models.CharField(max_length = 50)
    url = models.CharField(null = True,max_length =50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)
    column = models.ForeignKey(Column)

    def __unicode__(self):
        return self.name


	#class Meta:
	#	ordering = ['order']

class Inside(models.Model):
    name = models.CharField(max_length = 50)
    url = models.CharField(null = True,max_length =50)
    order = models.IntegerField()
    is_active = models.BooleanField(default = True)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return self.name







