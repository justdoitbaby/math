from django.contrib import admin
from Math.topnav.models import Column, Dpartments, Personal, Undergraduate, Postgraduate, Academic, Activity, Inside



admin.site.register(Column)
admin.site.register(Dpartments)
admin.site.register(Personal)
admin.site.register(Undergraduate)
admin.site.register(Postgraduate)
admin.site.register(Academic)
admin.site.register(Activity)
admin.site.register(Inside)
