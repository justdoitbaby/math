#coding:utf-8
from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from Math.topnav.models import Column, Dpartments, Personal, Undergraduate, Postgraduate, Academic, Activity, Inside


def topnav(request):
    column_list = Column.objects.all()
    dpartments_list = Dpartments.objects.all()
    personal_list = Personal.objects.all()
    undergraduate_list = Undergraduate.objects.all()
    postgraduate_list = Postgraduate.objects.all()
    academic_list = Academic.objects.all()
    activity_list = Activity.objects.all()
    inside_list = Inside.objects.all()

    return render_to_response("topnav.html",{"column_list":column_list,"dpartments_list":dpartments_list,"personal_list":personal_list,"undergraduate_list":undergraduate_list,"postgraduate":postgraduate_list,"academic_list":academic_list,"activity_list":activity_list,"inside_list":inside_list},RequestContext(request))

