# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('topnav', '0003_remove_inside_column'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='url',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='column',
            name='url',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='dpartments',
            name='url',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='inside',
            name='url',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
