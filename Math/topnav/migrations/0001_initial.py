# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Academic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Column',
            fields=[
                ('id', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Dpartments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('column', models.ForeignKey(to='topnav.Column')),
            ],
        ),
        migrations.CreateModel(
            name='Inside',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('column', models.ForeignKey(to='topnav.Column')),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Personnel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('column', models.ForeignKey(to='topnav.Column')),
            ],
        ),
        migrations.CreateModel(
            name='Postgraduate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('column', models.ForeignKey(to='topnav.Column')),
            ],
        ),
        migrations.CreateModel(
            name='Undergraduate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('order', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('column', models.ForeignKey(to='topnav.Column')),
            ],
        ),
        migrations.AddField(
            model_name='activity',
            name='column',
            field=models.ForeignKey(to='topnav.Column'),
        ),
        migrations.AddField(
            model_name='academic',
            name='column',
            field=models.ForeignKey(to='topnav.Column'),
        ),
    ]
