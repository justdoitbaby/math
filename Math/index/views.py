#coding=utf-8
from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import render, render_to_response, get_object_or_404

from django.views.decorators.cache import cache_page

from Math.academic.models import Lecture
from Math.news.models import News
from Math.index.models import image

@cache_page(60 * 15)
def index(request):
    lecture_list = Lecture.objects.all()[:5]
    news_list = News.objects.all()[:5]
    image_list = image.objects.all()
    return render_to_response("index.html",{"lecture_list":lecture_list,"news_list":news_list, "image_list": image_list},RequestContext(request))


