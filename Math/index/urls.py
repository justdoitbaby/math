#coding=utf-8
from django.conf.urls import include, url
from django.contrib import admin

from  Math.index import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$',views.index),
]


