#coding:utf-8
from django.db import models

class Aboutus(models.Model):
    title = models.CharField(max_length = 50)
    text = models.TextField()
    img = models.ImageField(upload_to = 'images')

    class Meta:
        verbose_name = '数学系介绍'
    
