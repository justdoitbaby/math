#coding:utf-8
from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from Math.aboutus.models import Aboutus
# Create your views here.

def aboutus(request):
    aboutus_list = Aboutus.objects.all()
    return render_to_response("aboutus.html",{"aboutus_list":aboutus_list},RequestContext(request))
    
