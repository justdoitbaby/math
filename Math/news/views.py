#coding=utf-8
from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from Math.news.models import News

def news(request):
    limit = 6
    news_list = News.objects.all()
    paginator = Paginator(news_list, limit)  # 实例化一个分页对象

    page = request.GET.get('page')  # 获取页码
    try:
        news_list = paginator.page(page)  # 获取某页对应的记录
    except PageNotAnInteger:  # 如果页码不是个整数
        news_list = paginator.page(1)  # 取第一页的记录
    except EmptyPage:  # 如果页码太大，没有相应的记录
        news_list = paginator.page(paginator.num_pages)  # 取最后一页的记录

    return render_to_response("news.html",{"news_list":news_list},RequestContext(request))
