# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20160314_0218'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='url',
            field=models.CharField(default=datetime.datetime(2016, 3, 14, 7, 26, 27, 155187, tzinfo=utc), max_length=50),
            preserve_default=False,
        ),
    ]
