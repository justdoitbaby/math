# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField()),
                ('title', models.CharField(max_length=100)),
                ('text', models.TextField()),
                ('img', models.ImageField(upload_to=b'')),
            ],
            options={
                'ordering': ['date'],
            },
        ),
    ]
