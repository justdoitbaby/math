#coding=utf-8
from django.db import models

class News(models.Model):
    date = models.DateTimeField()
    title = models.CharField(max_length = 100)
    url = models.CharField(max_length = 50)
    text = models.TextField()
    img = models.ImageField(upload_to ='news/images')

    class Meta:
        ordering = ['-date']
        verbose_name = '数学系新闻'

    def __unicode__(self):
        return self.title





