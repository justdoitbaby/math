#coding:utf-8
from django.db import models


class Employment(models.Model):
    id = models.CharField(primary_key = True,max_length =10)
    text = models.TextField()
    detail_url = models.CharField(max_length = 100)
    apply_url = models.CharField(max_length = 100)

    class Meta:
        ordering = ['id']
        verbose_name = '招贤纳士'


    def __unicode__(self):
        return self.text


