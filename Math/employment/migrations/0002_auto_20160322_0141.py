# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employment', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='employment',
            options={'ordering': ['id'], 'verbose_name': '\u62db\u8d24\u7eb3\u58eb'},
        ),
    ]
