# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Employment',
            fields=[
                ('id', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('text', models.TextField()),
                ('detail_url', models.CharField(max_length=100)),
                ('apply_url', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
    ]
