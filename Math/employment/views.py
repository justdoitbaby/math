from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from Math.employment.models import Employment

def employment(request):
    employment_list = Employment.objects.all()
    return render_to_response("employment.html",{"employment_list":employment_list},RequestContext(request))

